jQuery(document).ready(function() {
	$('a.scroll-link').on('click', function(e) {
		e.preventDefault();

		var elementСlass = $(this).attr('href').replace('#', '.');
		var scrollTo = (elementСlass != '.top-content') ? ($(elementСlass + '-container').offset().top) : 0;

		if($(window).scrollTop() != scrollTo) {
			$('html, body').stop().animate({scrollTop: scrollTo}, 1000);
		}
	});

	createBackground('.top-content', 60);
	
	new WOW().init();
});

jQuery(window).load(function() {
	$(".loader-img").fadeOut();
	$(".loader").delay(1000).fadeOut("slow");
});