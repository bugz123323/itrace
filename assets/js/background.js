var createBackground = function(element, speed) {
	var parent = $(element);
	var background = parent.find('.background')[0];
	var canvas = parent.find('.canvas')[0];
	var context = canvas.getContext('2d');
	var yPositions = Array(300).join(0).split('');

	background.width = canvas.width = parent.width();
	background.height = canvas.height = parent.outerHeight();

	return setInterval(function () {
		context.fillStyle = 'rgba(0, 0, 0, .05)';
		context.fillRect(0, 0, context.canvas.width, context.canvas.height);
		context.fillStyle = 'rgba(0, 255, 0, .15)';
		context.font = '10pt Georgia';

		yPositions.map(function(y, index) {
			context.fillText(String.fromCharCode(1e2 + Math.random() * 33), (index * 10) + 10, y);
			yPositions[index] = (y > 100 + Math.random() * 1e4) ? 0 : (y + 10);
		});
	}, speed);
};